import {Router} from 'express';
import axios from 'axios';
import {model} from 'mongoose';
const routes = Router();
routes.get('/', (req,res) => {
    const Joke = model('joke');
    axios.get('https://geek-jokes.sameerkumar.website/api')
    .then(async response => {
        let joke = await Joke.findOne({joke: response.data});
        if(!joke) {
                joke = await new Joke({joke: response.data}).save();
        }
        res.json(joke);
        res.end();
    });
});
export default routes;