import mongoose from 'mongoose';
import './schema/jokeSchema';
mongoose.connect('mongodb://localhost:27017/yeet');

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));
db.once("open", function(callback) {
    console.log("Connection succeeded.");
});

export default db;