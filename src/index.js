import http from 'http';
import express from 'express';
import cors from 'cors';
import rootRoutes from './routes/rootRoutes';
import db from './mongoVoodoo';
const app = express();
const server = http.createServer(app);
app.use(express.json());

app.use(cors());
app.use('/', rootRoutes);


server.listen(3001);
export default {app, server, db};