import {model} from 'mongoose';

model('joke', {joke: String, upvotes: {type: Number, default: 0}, downvotes: {type: Number, default: 0}});