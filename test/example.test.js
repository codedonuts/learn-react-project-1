import request from 'supertest';
import index from '../src/index';
import axios from 'axios';
it('should work',async () => {
    axios.get = jest.fn().mockImplementation(async () => ({data: 'ha-ha i am funny'}));
    const rs = await request(index.app).get('/').send();
    expect(rs.body.joke).toMatchSnapshot();
});


afterAll(async () => {
    await index.server.close();
    await index.db.close()
  });